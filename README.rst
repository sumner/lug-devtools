lug-devtools
============

:Author: `Sumner Evans`_
:Date: 2018-04-19
:Rendered: `4:3`__, `16:9`__, `16:10`__

.. _Sumner Evans: https://github.com/sumnerevans
__ ./out/lug-devtools-43.pdf
__ ./out/lug-devtools-169.pdf
__ ./out/lug-devtools-1610.pdf

A LUG talk about useful development tools including linters, the language server
protocol, and Python virtual environments.

Repo Organization
-----------------

- ``lug-devtools.rst`` - the reStructuredText source of this presentation
- ``out/*.pdf`` - the rendered presentation for various aspect ratios
