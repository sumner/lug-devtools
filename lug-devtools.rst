Development Tools
#################
An overview of a bunch of useful development tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Author: Sumner Evans
:Date: 2018-04-19

Linters
=======

**Linting is the process of running a program that will analyse code for
potential errors.**

- Linters are great for catching errors as you are writing a program, rather
  than waiting for compile or runtime.
- Linters can catch errors ranging from missing semicolons to potentially
  dangerous code practices such as ``==`` versus ``===`` in JavaScript.
- There are linters for almost every language under the sun, including English!

Linters: Useful Linters
=======================

There is a really good list on the ALE README: https://github.com/w0rp/ale. But
here's a list of my favourites:

- ``clang``, ``clangcheck`` and ``clang-format`` for C++
- ``stylelint`` for CSS
- ``brittany`` for Haskell
- ``eslint`` for JavaScript and JSX
- ``pyls`` for Python
- ``rls`` for Rust
- ``idris`` for Idris
- ``vale`` for English prose

Linters: Useful Fixers
======================

Some linters can also automatically format code to match a style guide such as
PEP-8. Sometimes, the best linter and best fixer for a language are not the same
program. Here are my favourite fixers:

- ``clang-format`` for C++
- ``stylelint`` for CSS
- ``brittany`` for Haskell
- ``eslint`` for JavaScript and JSX
- ``isort`` and ``yapf`` for Python
- ``rustfmt`` for Rust

Language Server Protocol
========================

The `Language Server Protocol (LSP)`__ is a protocol specifying how language
servers and language clients communicate with one another. The protocol was
originally created for VSCode, but clients have been made for tons of other
editors.

__ https://microsoft.github.io/language-server-protocol/

- **Language servers** are implemented for a language and can provide features
  such as

  - Autocompletion
  - Linting and diagnostics
  - Code hints
  - Jump to definition
  - Find references
  - Find symbols
  - Autoformatting

- **Language clients** are implemented for an editor/IDE and expose the features
  in an editor-specific manner.

Best Language Servers
=====================

Check the list at http://langserver.org for the most up-to-date list. Here are
some of my favourites:

- ``clangd`` for C++
- ``vscode-css-languageserver`` for CSS
- ``vscode-html-languageserver`` for HTML
- ``javascript-typescript-langserver`` for JavaScript, JSX, and TypeScript
- ``pyls`` for Python
- ``rls`` for Rust

Python Virtual Environments
===========================

``virtualenv`` is a tool to create siloed, isolated Python environments. I
recommend installing ``virtualenvwrapper`` for a bunch of tools to help manage
your virtual environments.

You can have different versions of dependencies per-project. Helps reduce
clutter in ``.local`` and ``/usr/bin``.

This is extremely useful if you want to do development on some package that you
also have installed. You don't want to override your system or user's package
with an unstable development version of a package!

Questions?
==========

- **Did I miss anything?**

- **What cool devtools do you use?**
